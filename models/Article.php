<?php

namespace app\models;

use yii\db\ActiveRecord;

class Article extends ActiveRecord
{
    public static  function  tableName()
    {
        return 'article_tab';
    }

    public static  function  getAll(){
        $data = self::find()->all();
        return $data;
    }

    public static  function  getArticle($id) {
        $data = self::find()->where(['article_id'=>$id])->one();
        return $data;
    }

}