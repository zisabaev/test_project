<?php

namespace app\controllers;

use app\models\EntryForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Article;

class IndexController extends Controller
{
    public function actionIndex()
    {
        $arr = Article::getAll();

        return $this->render('index',['arr'=>$arr]);
    }


    public function actionArticle($id){
        $arr = Article::getArticle($id);
        return $this->render('article',['arr'=>$arr]);
    }

    public function actionEntry()
    {
        $model = new EntryForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            // ������ � $model ������ ���������

            // ������ ���-�� �������� � $model ...

            return $this->render('entry-confirm', ['model' => $model]);
        } else {
            // ���� �������� ������������ ������ ���, ���� ���� ������ � ������
            return $this->render('entry', ['model' => $model]);
        }
    }

}
